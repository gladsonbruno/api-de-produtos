package com.example.api.controller;

import com.example.api.entity.Produto;
import com.example.api.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1")
public class ProdutoController {

    Logger logger = LoggerFactory.getLogger(ProdutoController.class);

    @Autowired
    private ProdutoService produtoService;

    @GetMapping(path = "produtos")
    public ResponseEntity<List<Produto>> findAll() {
        return new ResponseEntity<>(produtoService.getAll(), HttpStatus.OK);
    }

    @GetMapping(path = "produtos/{id}")
    public ResponseEntity<Produto> findById(@PathVariable("id") int id) {
        Optional<Produto> produto = produtoService.getById(id);

        if(produto.isPresent()) {
            return new ResponseEntity<>(produto.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping(path = "produtos")
    public ResponseEntity<Produto> addProduto(@RequestBody Produto produto) {

        return new ResponseEntity<>(produtoService.save(produto), HttpStatus.CREATED);
    }

    @PutMapping(path="produtos/{id}")
    public ResponseEntity<Produto> updateProduto(@PathVariable("id") int id, @RequestBody Produto produto) {

        return new ResponseEntity<>(produtoService.save(produto), HttpStatus.OK);
    }

    @DeleteMapping(path="produtos/{id}")
    public ResponseEntity<?> deleteProduto(@PathVariable("id") int id) {
        try {
            produtoService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
