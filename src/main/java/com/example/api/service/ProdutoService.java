package com.example.api.service;

import com.example.api.entity.Produto;
import com.example.api.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository repository;

    public List<Produto> getAll() {
        return repository.findAll();
    }

    public Optional<Produto> getById(int id) {
        return repository.findById(id);
    }

    public Produto save(Produto produto) {
        return repository.save(produto);
    }

    public void deleteById(int id) {
        repository.deleteById(id);
    }

}
